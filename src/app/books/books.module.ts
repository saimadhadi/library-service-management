import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooklistComponent } from './booklist/booklist.component';
import { BookComponent } from './book/book.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [BooklistComponent, BookComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class BooksModule { }
