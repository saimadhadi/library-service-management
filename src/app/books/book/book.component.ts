import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '../book';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input()
  isEdit : boolean = false;

  @Input()
  book : Book;

  @Output()
  saveEvent = new EventEmitter<any>();

  domainURL = environment.baseURI;
  
  constructor(private http:HttpClient , private toastr : ToastrService) { }

  ngOnInit(): void {
  }

  saveBook() {
    console.log("in boook componenet ",this.book);
    if(this.isEdit){
      console.log("Edit book");
      this.http.put(this.domainURL+"library/books/"+this.book.bookId, this.book).subscribe(res => {
        console.log("book updated");
        this.saveEvent.emit("success");
      },error => this.toastr.error("Book can not be updated"));
    }
    else {
      console.log("Create book");
      this.http.post(this.domainURL+"library/books/", this.book).subscribe(res => {
        console.log("book created");
        this.saveEvent.emit("success");
      },error => this.toastr.error("Book can not be created"));
    }
  }

}
