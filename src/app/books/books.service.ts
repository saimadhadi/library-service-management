import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from './book';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  domainURL = environment.baseURI;

  constructor(private http:HttpClient) { }

  getBooks() :Observable<Book[]>{
    return this.http.get<Book[]>(this.domainURL+"library/books").pipe(
      catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);
  }
}
