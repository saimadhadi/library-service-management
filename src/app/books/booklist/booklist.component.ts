import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '../book';
import { environment } from 'src/environments/environment';
import { BooksService } from '../books.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.css']
})
export class BooklistComponent implements OnInit {

  books : Book[] = [] ;
  isEdit : boolean = false;
  dialogTitle : string;
  selectedBook : Book;
  domainURL = environment.baseURI;
  
  constructor(private http:HttpClient,private booksService:BooksService, private toastr : ToastrService) {
      this.selectedBook=new Book();
   }

  ngOnInit(): void {
    this.booksService.getBooks().subscribe( (res:Book[]) => {
      this.books = res;
      console.log(res);
    },error => this.toastr.error("Books could not be fetched"));
  }

  delete(book : Book){
    console.log(book);
    this.http.delete(this.domainURL+"library/books/"+book.bookId).subscribe(res => {
      console.log("Book deleted");
      this.booksService.getBooks().subscribe( (res:Book[]) => {
        this.books = res;
        console.log(res);   
      });
    }, error => this.toastr.error("Book cannot be deleted"))
  }

  create() {
    console.log("create book in BookList");
    this.selectedBook = new Book();
    this.isEdit = false;
    this.dialogTitle = "Add Book";
  }

  update(book : Book) {
    console.log("update book in BookList");
    this.selectedBook = book;
    this.isEdit = true;
    this.dialogTitle = "Edit Book";
  }

 saveBookAck(event) {
    document.getElementById("closePopup").click();
    this.booksService.getBooks().subscribe(data => this.books = data, error => this.toastr.error("Book could not be fetched"));
    console.log(event);
  }

}
