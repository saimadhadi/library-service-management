export class Book {
    public bookId : number;
    public bookName : string;
    public bookAuthor : string;
    public bookGenre : string;
    public bookDescription : string
}