import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserbooksComponent } from './userbooks/userbooks.component';
import { FormsModule } from '@angular/forms';
import { UsersModule } from '../users/users.module';
import { BooksModule } from '../books/books.module';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [UserbooksComponent],
  imports: [
    CommonModule,
    FormsModule,
    BooksModule,
    UsersModule,
    BrowserModule
  ]
})
export class CirculationModule { }
