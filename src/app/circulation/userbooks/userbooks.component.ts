import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from 'src/app/books/book';
import { LibraryUser } from '../LibraryUser';
import { User } from 'src/app/users/user';
import { CirculationService } from '../circulation.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-userbooks',
  templateUrl: './userbooks.component.html',
  styleUrls: ['./userbooks.component.css']
})
export class UserbooksComponent implements OnInit {

  userId : number=1;
  bookId : number=1;
  books : Book[] = [] ;
  libraryUser : LibraryUser;
  showTable : boolean;
  domainURL = environment.baseURI;

  constructor(private http : HttpClient,private circulationService :CirculationService, private toastr : ToastrService) {
    this.libraryUser = new LibraryUser();
    this.libraryUser.userModel = new User();
    this.libraryUser.bookModelList = [];
   }

  ngOnInit(): void {
    this.showTable= false;
  }

  getLibraryUser() : void {
    this.circulationService.getBooks(this.userId).subscribe((res:LibraryUser) =>{  
      console.log("result :",res);
      this.showTable=true;
      this.libraryUser=res;
      console.log("libraryUser :",this.libraryUser.bookModelList, this.libraryUser.userModel);
    },error => this.toastr.error("Library User Model could not be fetched"))
  }

  remove(bookId:number) : void {
    this.http.delete(this.domainURL+"library/users/"+this.userId+"/books/"+bookId).subscribe(res => {
      console.log("Book has been removed");
      this.circulationService.getBooks(this.userId).subscribe((res:LibraryUser) =>{
        this.libraryUser=res;
    },error => this.toastr.error("Library User Model could not be fetched"))
    }, error => this.toastr.error("Book cannot be deleted"))
  }

  addBook() : void {
    this.http.post(this.domainURL+"library/users/"+this.userId+"/books/"+this.bookId,new Book()).subscribe(res => {
      console.log("Book has been added");
      this.circulationService.getBooks(this.userId).subscribe((res:LibraryUser) =>{
        this.libraryUser=res;
        document.getElementById("closePopup").click();
    },error => {this.toastr.error("Library User Model could not be fetched");document.getElementById("closePopup").click();})
    }, error => {this.toastr.error("Book cannot be Added");document.getElementById("closePopup").click()})
  }


}
