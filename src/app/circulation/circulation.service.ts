import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { LibraryUser } from './LibraryUser';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CirculationService {

  domainURL = environment.baseURI;

  constructor(private http:HttpClient) { }

  getBooks(userId) : Observable<LibraryUser>{
    return this.http.get<LibraryUser>(this.domainURL+"library/users/"+userId).pipe(
      catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);
  }
}
