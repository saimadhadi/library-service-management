import { Book } from '../books/book';
import { User } from '../users/user';

export class LibraryUser {
    public userModel : User;
    public bookModelList : Book[];
}