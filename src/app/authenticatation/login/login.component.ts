import { Component, OnInit } from '@angular/core';
import { LoginUser } from '../loginuser';
import { HttpClient } from '@angular/common/http';
import { JwtToken } from '../jwtToken';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser : LoginUser;

  constructor(private http:HttpClient, private router : Router,private auth:AuthService) {
    this.loginUser = new LoginUser();
   }

  ngOnInit(): void {
  }

  logUser() : void {
    console.log(this.loginUser);
    this.auth.loginUser(this.loginUser).subscribe((res:JwtToken)=>{
      console.log(res);
      localStorage.setItem('token',res.jwtToken);
      this.router.navigate(['books']);
    })
  
  }

}
