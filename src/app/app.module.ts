import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { BooksModule } from './books/books.module';
import { UsersModule } from './users/users.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CirculationModule } from './circulation/circulation.module';
import { AuthenticatationModule } from './authenticatation/authenticatation.module';
import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './authenticatation/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BooksModule,
    UsersModule,
    CirculationModule,
    CommonModule,
    AuthenticatationModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut : 5000,
      positionClass : 'toast-bottom-right',
      preventDuplicates : false
    })
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
