import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  domainURL = environment.baseURI;

  constructor(private http:HttpClient) { }

  getUsers() :Observable<User[]>{
    return this.http.get<User[]>(this.domainURL+"library/users").pipe(
      catchError(this.errorHandler));
  }

  errorHandler(error : HttpErrorResponse){
    return Observable.throw(error.message);
  }
}
