import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../user';
import { environment } from 'src/environments/environment';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  users : User[] =[] ;
  isEdit : boolean = false;
  dialogTitle : string;
  selectedUser : User;
  domainURL = environment.baseURI;
  
  constructor(private http:HttpClient,private userService:UserService, private toastr : ToastrService) {
      this.selectedUser=new User();
   }

  ngOnInit(): void {
    this.userService.getUsers().subscribe( (res:User[]) => {
      this.users = res;
      console.log(res);
    },error => this.toastr.error("Users could not be fetched"));
  }

  delete(user : User){
    this.http.delete(this.domainURL+"library/users/"+user.userId).subscribe(res => {
      console.log("User deleted");
      this.userService.getUsers().subscribe( (res:User[]) => {
        this.users = res;
        console.log(res);
      }, error => {console.log("jsksk");this.toastr.error("User cannot be deleted")});
    })
  }

  create() {
    console.log("create user in UserList");
    this.selectedUser = new User();
    this.isEdit = false;
    this.dialogTitle = "Add User";
  }
  
  update(user : User) {
    console.log("update user in UserList");
    this.selectedUser = user;
    this.isEdit = true;
    this.dialogTitle = "Edit User";
  }

 saveBookAck(event) {
    document.getElementById("closePopup").click();
    this.userService.getUsers().subscribe(data => this.users = data,  error => this.toastr.error("User could not be fetched"));
    console.log(event);
  }

}
