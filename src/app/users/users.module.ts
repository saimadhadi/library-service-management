import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { UserComponent } from './user/user.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserlistComponent, UserComponent],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class UsersModule { }
