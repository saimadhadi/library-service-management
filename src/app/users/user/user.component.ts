import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../user';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  isEdit : boolean = false;

  @Input()
  user : User;

  @Output()
  saveEvent = new EventEmitter<any>();

  domainURL = environment.baseURI;
  
  constructor(private http:HttpClient, private toastr : ToastrService) { }

  ngOnInit(): void {
  }

  saveUser() {
    if(this.isEdit){
      console.log("Edit User");
      this.http.put(this.domainURL+"library/users/"+this.user.userId, this.user).subscribe(res => {
        console.log("User updated");
        this.saveEvent.emit("success");
      },error => this.toastr.error("User can not be updated"));
    }
    else {
      this.http.post(this.domainURL+"library/users/", this.user).subscribe(res => {
        console.log("user created");
        this.saveEvent.emit("success");
      },error => this.toastr.error("User can not be created"));
    }
  }

}
