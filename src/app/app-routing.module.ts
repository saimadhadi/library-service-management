import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BooklistComponent} from './books/booklist/booklist.component';
import {UserlistComponent} from './users/userlist/userlist.component';
import { UserbooksComponent } from './circulation/userbooks/userbooks.component';
import { LoginComponent } from './authenticatation/login/login.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [ 
  {path : 'books',component : BooklistComponent,canActivate : [AuthGuard]},
  {path : 'users',component : UserlistComponent,canActivate : [AuthGuard]},
  {path : 'circulations',component : UserbooksComponent,canActivate : [AuthGuard]},
  {path : 'login',component : LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
